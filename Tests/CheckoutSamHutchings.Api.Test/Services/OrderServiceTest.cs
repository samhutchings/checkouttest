using CheckoutSamHutchings.Api.Models;
using CheckoutSamHutchings.Api.Services;
using Microsoft.Extensions.Caching.Memory;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckoutSamHutchings.Api.Tests.Services
{
    [TestFixture]
    public class OrderServiceTest
    {
        private static readonly string __existingOrderId = "existing-order";

        private IMemoryCache _cache;
        private Order _existingOrder;

        private OrderRepository _service;

        [SetUp]
        public void SetUp()
        {
            _cache = new MemoryCache(new MemoryCacheOptions());

            _existingOrder = new Order
            {
                Items = new List<Item>
                {
                    new Item { ProductId = "12333", Quantity = 1, UnitPrice = 10.0m },
                }
            };

            _cache.Set(__existingOrderId, _existingOrder);

            _service = new OrderRepository(_cache);
        }

        [Test]
        public async Task OrderReturnsOrderIfOneExists()
        {
            var order = await _service.GetAsync(__existingOrderId);

            Assert.That(order, Is.EqualTo(_existingOrder));
        }

        [Test]
        public async Task OrderReturnsNewOrderIfNoneExists()
        {
            var order = await _service.GetAsync("new-order");

            Assert.That(order, Is.Not.Null);
        }

        [Test]
        public async Task UpdateOrderUpdatesWhenOrderExists()
        {
            var updatedOrder = new Order
            {
                Items = new List<Item>
                {
                    new Item { ProductId = "1234", Quantity = 1, UnitPrice = 10.0m },
                    new Item { ProductId = "12345", Quantity = 5, UnitPrice = 30.0m },
                }
            };

            await _service.UpdateAsync(__existingOrderId, updatedOrder);

            Assert.That(_cache.Get<Order>(__existingOrderId), Is.EqualTo(updatedOrder));
        }

        [Test]
        public async Task UpdateCreatesOrderWhenNoneExists()
        {
            var newOrder = new Order
            {
                Items = new List<Item>
                {
                    new Item { ProductId = "1234", Quantity = 1, UnitPrice = 10.0m },
                    new Item { ProductId = "12345", Quantity = 5, UnitPrice = 30.0m },
                }
            };

            await _service.UpdateAsync("new-order", newOrder);

            Assert.That(_cache.Get<Order>("new-order"), Is.EqualTo(newOrder));
        }

        [Test]
        public async Task ClearRemovesOrderIfExists()
        {
            await _service.ClearAsync(__existingOrderId);

            Assert.That(_cache.Get<Order>(__existingOrderId), Is.Null);
        }

        [Test]
        public async Task AddItemAddsItemToOrder()
        {
            var item = new Item { ProductId = "1234", Quantity = 1, UnitPrice = 40 };

            await _service.AddItemAsync(__existingOrderId, item);

            Assert.That(_existingOrder.Items.Contains(item));
        }

        [Test]
        public async Task RemoveItemRemovesItemFromOrder()
        {
            _existingOrder.Items.Add(new Item { ProductId = "1234", Quantity = 1, UnitPrice = 40 });

            await _service.RemoveItemAsync(__existingOrderId, "1234");

            Assert.That(_existingOrder.Items.Any(x => x.ProductId == "1234"), Is.False);
        }

        [Test]
        public async Task UpdateQuantitiesUpdatesMatchingItems()
        {
            _existingOrder.Items.Add(new Item { ProductId = "1234", Quantity = 1, UnitPrice = 40 });
            _existingOrder.Items.Add(new Item { ProductId = "12345", Quantity = 1, UnitPrice = 50 });

            var newQuantities = new[]
            {
                new Item { ProductId = "12333", Quantity = 2},
                new Item { ProductId = "1234", Quantity = 3},
            };

            await _service.UpdateQuantitiesAsync(__existingOrderId, newQuantities);

            Assert.That(_existingOrder.Items.SingleOrDefault(x => x.ProductId == "12333").Quantity, Is.EqualTo(2));
            Assert.That(_existingOrder.Items.SingleOrDefault(x => x.ProductId == "1234").Quantity, Is.EqualTo(3));
            Assert.That(_existingOrder.Items.SingleOrDefault(x => x.ProductId == "12345").Quantity, Is.EqualTo(1));
        }
    }
}