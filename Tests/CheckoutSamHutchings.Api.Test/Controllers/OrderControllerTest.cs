using CheckoutSamHutchings.Api.Controllers;
using CheckoutSamHutchings.Api.Models;
using CheckoutSamHutchings.Api.Services;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CheckoutSamHutchings.Api.Tests.Controllers
{
    [TestFixture]
    public class OrderControllerTest
    {
        private static readonly string __existingOrderId = "existing-order";

        private IOrderRepository _orderService;
        private Order _existingOrder;

        private OrderController _controller;

        [SetUp]
        public void SetUp()
        {
            _existingOrder = new Order
            {
                Items = new List<Item>
                {
                    new Item { ProductId = "12333", Quantity = 1, UnitPrice = 10.0m },
                }
            };

            _orderService = Substitute.For<IOrderRepository>();
            _orderService.GetAsync(__existingOrderId).Returns(_existingOrder);

            _controller = new OrderController(_orderService);
        }

        [Test]
        public void GetCallsOrderService()
        {
            var result = _controller.Get(__existingOrderId);

            _orderService.Received(1).GetAsync(__existingOrderId);
        }

        [Test]
        public async Task GetReturnsOrderService()
        {
            var result = await _controller.Get(__existingOrderId);

            Assert.That(result, Has.Property("Value").EqualTo(_existingOrder));
        }

        [Test]
        public void PostCallsOrderService()
        {
            var order = new Order();

            var result = _controller.Post(__existingOrderId, order);

            _orderService.Received(1).UpdateAsync(__existingOrderId, order);
        }

        [Test]
        public async Task PostReturnsUpdatedOrder()
        {
            var order = new Order();

            _orderService.UpdateAsync(__existingOrderId, Arg.Any<Order>()).Returns(order);

            var result = await _controller.Post(__existingOrderId, order);

            Assert.That(result, Has.Property("Value").EqualTo(order));
        }
    }
}