﻿using CheckoutSamHutchings.Api.Controllers;
using CheckoutSamHutchings.Api.Models;
using CheckoutSamHutchings.Api.Services;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CheckoutSamHutchings.Api.Test.Controllers
{
    public class ItemControllerTest
    {
        private static readonly string __existingOrderId = "existing-order";

        private IOrderRepository _orderService;
        private Order _existingOrder;

        private ItemsController _controller;

        [SetUp]
        public void SetUp()
        {
            _existingOrder = new Order
            {
                Items = new List<Item>
                {
                    new Item { ProductId = "12333", Quantity = 1, UnitPrice = 10.0m },
                }
            };

            _orderService = Substitute.For<IOrderRepository>();
            _orderService.GetAsync(__existingOrderId).Returns(_existingOrder);

            _controller = new ItemsController(_orderService);
        }

        [Test]
        public async Task AddItemCallsOrderService()
        {
            var item = new Item { ProductId = "1234", Quantity = 2, UnitPrice = 30 };

            var result = await _controller.AddItem(__existingOrderId, item);

            await _orderService.Received(1).AddItemAsync(__existingOrderId, item);
        }

        [Test]
        public async Task AddItemReturnsUpdatedOrder()
        {
            var result = await _controller.AddItem(__existingOrderId, new Item { ProductId = "12345", Quantity = 5, UnitPrice = 50 });

            Assert.That(result, Has.Property("Value").EqualTo(_existingOrder));
        }

        [Test]
        public async Task RemoveItemCallsOrderService()
        {
            var result = await _controller.RemoveItem(__existingOrderId, new RemoveItemModel { ProductId = "12345" });

            await _orderService.Received(1).RemoveItemAsync(__existingOrderId, "12345");
        }

        [Test]
        public async Task RemoveItemReturnsUpdatedOrder()
        {
            var result = await _controller.RemoveItem(__existingOrderId, new RemoveItemModel { ProductId = "12345" });

            Assert.That(result, Has.Property("Value").EqualTo(_existingOrder));
        }

        [Test]
        public async Task UpdateQuantitiesCallsOrderService()
        {
            var updates = new[] { new Item { ProductId = "1234", Quantity = 2 } };

            var result = await _controller.Update(__existingOrderId, updates);

            await _orderService.Received(1).UpdateQuantitiesAsync(__existingOrderId, updates);
        }
        [Test]
        public async Task UpdateQuantitiesReturnsUpdatedOrder()
        {
            var updates = new[] { new Item { ProductId = "1234", Quantity = 2 } };

            var result = await _controller.Update(__existingOrderId, updates);

            Assert.That(result, Has.Property("Value").EqualTo(_existingOrder));
        }
    }
}
