using CheckoutSamHutchings.Api.Models;
using CheckoutSamHutchings.Api.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CheckoutSamHutchings.Api.Controllers
{
    [ProducesResponseType(typeof(BadRequestResult), 400)]
    [ProducesResponseType(typeof(NotFoundResult), 404)]
    [ProducesResponseType(typeof(StatusCodeResult), 500)]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderRepository _orderService;

        public OrderController(IOrderRepository orderService)
        {
            _orderService = orderService;
        }

        [HttpGet]
        [Route("api/[controller]/{id}")]
        [ProducesResponseType(typeof(Order), 200)]
        public async Task<ActionResult<Order>> Get(string id)
        {
            return await _orderService.GetAsync(id);
        }

        [HttpPost]
        [Route("api/[controller]/{id}")]
        [ProducesResponseType(typeof(Order), 200)]
        public async Task<ActionResult<Order>> Post(string id, Order order)
        {
            var updatedOrder = await _orderService.UpdateAsync(id, order);

            return updatedOrder;
        }

        [HttpDelete]
        [Route("api/[controller]/{id}")]
        [ProducesResponseType(typeof(OkResult), 200)]
        public async Task<ActionResult> Delete(string id)
        {
            await _orderService.ClearAsync(id);

            return Ok();
        }
    }
}
