﻿using Microsoft.AspNetCore.Mvc;

namespace CheckoutSamHutchings.Api.Controllers
{
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Redirect("/docs");
        }
    }
}