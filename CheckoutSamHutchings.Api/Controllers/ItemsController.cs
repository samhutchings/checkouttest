﻿using CheckoutSamHutchings.Api.Models;
using CheckoutSamHutchings.Api.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CheckoutSamHutchings.Api.Controllers
{
    [ProducesResponseType(typeof(BadRequestResult), 400)]
    [ProducesResponseType(typeof(NotFoundResult), 404)]
    [ProducesResponseType(typeof(StatusCodeResult), 500)]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly IOrderRepository _orderService;

        public ItemsController(IOrderRepository orderService)
        {
            _orderService = orderService;
        }

        [HttpPost]
        [Route("api/order/items/add/{id}")]
        [ProducesResponseType(typeof(Order), 200)]
        public async Task<ActionResult<Order>> AddItem(string id, Item item)
        {
            await _orderService.AddItemAsync(id, item);

            return await _orderService.GetAsync(id);
        }

        [HttpPost]
        [Route("api/order/items/remove/{id}")]
        [ProducesResponseType(typeof(Order), 200)]
        public async Task<ActionResult<Order>> RemoveItem(string id, RemoveItemModel model)
        {
            await _orderService.RemoveItemAsync(id, model.ProductId);

            return await _orderService.GetAsync(id);
        }

        [HttpPost]
        [Route("api/order/items/update/{id}")]
        [ProducesResponseType(typeof(Order), 200)]
        public async Task<ActionResult<Order>> Update(string id, IEnumerable<Item> updates)
        {
            await _orderService.UpdateQuantitiesAsync(id, updates);

            return await _orderService.GetAsync(id);
        }
    }
}
