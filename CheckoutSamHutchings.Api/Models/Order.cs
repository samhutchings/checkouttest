using System.Collections.Generic;

namespace CheckoutSamHutchings.Api.Models
{
    public class Order
    {
        public IList<Item> Items { get; set; } = new List<Item>();
    }
}