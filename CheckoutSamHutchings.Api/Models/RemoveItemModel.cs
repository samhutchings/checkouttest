﻿namespace CheckoutSamHutchings.Api.Models
{
    public class RemoveItemModel
    {
        public string ProductId { get; set; }
    }
}
