using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CheckoutSamHutchings.Api.Models
{
    public class Item : IValidatableObject
    {
        [Required(ErrorMessage = "Please enter a product ID")]
        public string ProductId { get; set; }

        [Required(ErrorMessage = "Please enter a quantity for this item")]
        public int? Quantity { get; set; }

        [Required(ErrorMessage = "Please enter a unit price for this item")]
        public decimal? UnitPrice { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Quantity < 1)
                yield return new ValidationResult("The quantity must be one or more");
        }
    }
}