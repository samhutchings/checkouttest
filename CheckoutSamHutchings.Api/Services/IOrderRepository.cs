using CheckoutSamHutchings.Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CheckoutSamHutchings.Api.Services
{
    public interface IOrderRepository
    {
        Task<Order> GetAsync(string id);

        Task<Order> UpdateAsync(string id, Order order);

        Task ClearAsync(string id);

        Task AddItemAsync(string id, Item item);

        Task RemoveItemAsync(string id, string productId);

        Task UpdateQuantitiesAsync(string id, IEnumerable<Item> updates);
    }
}