﻿using CheckoutSamHutchings.Api.Models;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckoutSamHutchings.Api.Services
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IMemoryCache _orderCache;

        public OrderRepository(IMemoryCache cache)
        {
            _orderCache = cache;
        }

        public async Task<Order> GetAsync(string id)
        {
            var order = _orderCache.Get<Order>(id);

            if (order != null)
                return await Task.FromResult(order);

            order = new Order();

            _orderCache.Set(id, order);

            return await Task.FromResult(order);
        }

        public async Task<Order> UpdateAsync(string id, Order order)
        {
            _orderCache.Set(id, order);

            return await Task.FromResult(_orderCache.Get<Order>(id));
        }

        public Task ClearAsync(string id)
        {
            _orderCache.Remove(id);

            return Task.CompletedTask;
        }

        public async Task AddItemAsync(string id, Item item)
        {
            var order = await GetAsync(id);

            if (order.Items.Any(x => x.ProductId == item.ProductId))
                throw new ArgumentException($"Product {item.ProductId} already exists on order {id}");

            order.Items.Add(item);

            _orderCache.Set(id, order);
        }

        public async Task RemoveItemAsync(string id, string productId)
        {
            var order = await GetAsync(id);
            var item = order.Items.SingleOrDefault(x => x.ProductId == productId);

            if (item == null)
                throw new ArgumentException($"Product {productId} does not exist on order {id}");

            order.Items.Remove(item);

            _orderCache.Set(id, order);
        }

        public async Task UpdateQuantitiesAsync(string id, IEnumerable<Item> updates)
        {
            var order = await GetAsync(id);

            foreach (var update in updates)
            {
                var item = order.Items.SingleOrDefault(x => x.ProductId == update.ProductId);

                if (item == null)
                    throw new ArgumentException($"Product {update.ProductId} does not exist on order {id}");

                item.Quantity = update.Quantity;
            }

            _orderCache.Set(id, order);
        }
    }
}
