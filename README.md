# Checkout.com Engineering Test Jan 2019
### Sam Hutchings

All CLI commands assume a shell in the root CheckoutSamHutchings directory.

## Prerequisites

.NET Core SDK 2.2

## Build

```
$ dotnet build
```

## Test
```
$ dotnet test Tests/CheckoutSamHutchings.Api.Test/CheckoutSamHutchings.Api.Test.csproj
```

## Run

### To run API:

The API runs on port 5000, so will need that port to be free on localhost. If you browse to http://localhost:5000 you will see the Swagger documentation and be able to try the endpoints.

```
$ dotnet run -p CheckoutSamHutchings.Api/CheckoutSamHutchings.Api.csproj
```


### To run MVC site

This requires the API to be running and listening on port 5000. This site runs on port 5100 so will need that to be free on localhost.
```
$ dotnet run -p CheckoutSamHutchings.Web/CheckoutSamHutchings.Web.csproj
```

## Notes

Approximately 4-5 hours spent. Would probably use a user ID for the order ID, if there was an auth provider hooked up. Error handling and dependency injection/testing for the client library could be improved given more time.