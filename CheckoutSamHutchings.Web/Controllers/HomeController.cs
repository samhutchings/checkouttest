﻿using CheckoutSamHutchings.ClientLibrary.Models;
using CheckoutSamHutchings.ClientLibrary.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CheckoutSamHutchings.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly string _orderId;

        public HomeController(IConfiguration configuration)
        {
            _orderService = new OrderService(configuration["OrderServiceUrl"]);
            _orderId = configuration["OrderId"];
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var order = await _orderService.GetAsync(_orderId);

            return View(order);
        }

        [HttpGet]
        public IActionResult AddItem()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddItem(Item model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var order = await _orderService.AddItemAsync(_orderId, model);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> RemoveItem(string id)
        {
            if (!ModelState.IsValid)
                return View(id);

            var order = await _orderService.RemoveItemAsync(_orderId, id);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> UpdateOrder()
        {
            var order = await _orderService.GetAsync(_orderId);

            return View(order);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateOrder(Order model)
        {
            if (!ModelState.IsValid)
                return View(model);

            await _orderService.UpdateAsync(_orderId, model);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> ClearOrder()
        {
            await _orderService.ClearAsync(_orderId);

            return RedirectToAction("Index");
        }
    }
}
