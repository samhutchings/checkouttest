using System.ComponentModel.DataAnnotations;

namespace CheckoutSamHutchings.ClientLibrary.Models
{
    public class Item
    {
        [Required(ErrorMessage = "Please enter a product ID")]
        public string ProductId { get; set; }

        [Required(ErrorMessage = "Please enter a quantity for this item")]
        public int? Quantity { get; set; }

        [Required(ErrorMessage = "Please enter a unit price for this item")]
        public decimal? UnitPrice { get; set; }
    }
}