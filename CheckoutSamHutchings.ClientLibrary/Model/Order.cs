using System.Collections.Generic;

namespace CheckoutSamHutchings.ClientLibrary.Models
{
    public class Order
    {
        public IList<Item> Items { get; set; } = new List<Item>();
    }
}