﻿using CheckoutSamHutchings.ClientLibrary.Models;
using CheckoutSamHutchings.ClientLibrary.Settings;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CheckoutSamHutchings.ClientLibrary.Services
{
    public class OrderService : IOrderService
    {
        private static readonly HttpClient _apiClient = new HttpClient();
        private readonly string _apiUrl;

        public OrderService(string apiUrl)
        {
            _apiUrl = apiUrl;
        }

        public async Task<Order> GetAsync(string id)
        {
            var response = await _apiClient.GetAsync($"{_apiUrl}/{ApiEndpoints.Order}/{id}");

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Could not get order. Status code: {response.StatusCode}, message: {response.Content}");

            return await response.Content.ReadAsAsync<Order>();
        }

        public async Task<Order> UpdateAsync(string id, Order order)
        {
            var response = await _apiClient.PostAsJsonAsync($"{_apiUrl}/{ApiEndpoints.Order}/{id}", order);

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Could not update order. Status code: {response.StatusCode}, message: {response.Content}");

            return await response.Content.ReadAsAsync<Order>();
        }

        public async Task ClearAsync(string id)
        {
            var response = await _apiClient.DeleteAsync($"{_apiUrl}/{ApiEndpoints.Order}/{id}");

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Could not delete order. Status code: {response.StatusCode}, message: {response.Content}");
        }

        public async Task<Order> AddItemAsync(string id, Item item)
        {
            var response = await _apiClient.PostAsJsonAsync($"{_apiUrl}/{ApiEndpoints.AddItem}/{id}", item);

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Could not add item to order. Status code: {response.StatusCode}, message: {response.Content}");

            return await response.Content.ReadAsAsync<Order>();
        }

        public async Task<Order> RemoveItemAsync(string id, string productId)
        {
            var response = await _apiClient.PostAsJsonAsync($"{_apiUrl}/{ApiEndpoints.RemoveItem}/{id}", new { ProductId = productId });

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Could not remove item from order. Status code: {response.StatusCode}, message: {response.Content}");

            return await response.Content.ReadAsAsync<Order>();
        }

    }
}
