﻿using CheckoutSamHutchings.ClientLibrary.Models;
using System.Threading.Tasks;

namespace CheckoutSamHutchings.ClientLibrary.Services
{
    public interface IOrderService
    {
        Task<Order> GetAsync(string id);

        Task ClearAsync(string id);

        Task<Order> AddItemAsync(string id, Item item);

        Task<Order> RemoveItemAsync(string id, string productId);

        Task<Order> UpdateAsync(string id, Order order);
    }
}
