﻿namespace CheckoutSamHutchings.ClientLibrary.Settings
{
    public static class ApiEndpoints
    {
        public static string Order = "api/order";

        public static string AddItem = "api/order/items/add";

        public static string RemoveItem = "api/order/items/remove";

        public static string UpdateItems = "api/order/items/update";
    }
}
